package br.com.senac.exemplo4;

import java.util.HashMap;
import java.util.List;

public class FolhaPagamento {

    private HashMap<Integer, Double> comissao = new HashMap<>();

    public FolhaPagamento() {

    }

    private final double TAXA_COMISSAO = 0.05;

    public void calcularComissao(List<Venda> lista) {

        for (Venda v : lista) {

            int codigo = v.getVendedor().getCodigo();

            if (!comissao.containsKey(codigo)) {
                comissao.put(codigo, v.getTotal() * TAXA_COMISSAO);
            } else {
                double comissaoParcial = comissao.get(codigo).doubleValue();
                comissaoParcial += (v.getTotal() * TAXA_COMISSAO);
                comissao.put(codigo, comissaoParcial);
            }
        }
    }

    public double getComissaoVendedor(Vendedor vendedor) {
        return this.comissao.get(vendedor.getCodigo()).doubleValue();
    }

}

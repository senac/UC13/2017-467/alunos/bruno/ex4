
package br.com.senac.exemplo4.test;

import br.com.senac.exemplo4.FolhaPagamento;
import br.com.senac.exemplo4.Produto;
import br.com.senac.exemplo4.Venda;
import br.com.senac.exemplo4.Vendedor;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class FolhaPagamentoTest {
    
    public FolhaPagamentoTest() {
    }
    
    @Test
    public void deveCalcularComissaoParaBruno(){
        
        Vendedor vendedor = new Vendedor(1, "Bruno");
        Produto produto = new Produto(1, "Vodka");
        
        
        Venda venda1 = new Venda(vendedor) ;
        venda1.adicionarItem(produto , 1 , 10 ) ;
        
        Venda venda2 = new Venda(vendedor);
        venda2.adicionarItem(produto, 1, 10);
        
        List<Venda> listaVendas = new ArrayList<>() ; 
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        FolhaPagamento folhaPagamento = new FolhaPagamento() ; 
        folhaPagamento.calcularComissao(listaVendas) ; 
        double comissao = folhaPagamento.getComissaoVendedor(vendedor);
        
        assertEquals(1, comissao , 0.01);
       
    }
    
    @Test
    public void deveCalcularComissaoParaBrunoECr7(){
        
        Vendedor jose = new Vendedor(1, "Bruno");
        Vendedor miguel = new Vendedor(2, "Cr7");
        
        Produto produto = new Produto(1, "Vodka");
        
        
        Venda venda1 = new Venda(jose) ;
        venda1.adicionarItem(produto , 10 , 10 ) ;
        
        Venda venda2 = new Venda(miguel);
        venda2.adicionarItem(produto, 10, 20);
        
        List<Venda> listaVendas = new ArrayList<>() ; 
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        FolhaPagamento folhaPagamento = new FolhaPagamento() ; 
        folhaPagamento.calcularComissao(listaVendas) ; 
        double comissaoJose = folhaPagamento.getComissaoVendedor(jose) ; 
        
        assertEquals(5.0, comissaoJose , 0.01);
      
        
    }
  
}

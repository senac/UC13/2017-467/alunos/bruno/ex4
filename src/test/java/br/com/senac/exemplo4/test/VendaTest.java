
package br.com.senac.exemplo4.test;

import br.com.senac.exemplo4.Produto;
import br.com.senac.exemplo4.Venda;
import br.com.senac.exemplo4.Vendedor;
import org.junit.Test;
import static org.junit.Assert.*;


public class VendaTest {
    
    public VendaTest() {
    }
    
    @Test
    public void deveSomaTotalNota(){
        
        Venda venda = new Venda(new Vendedor(1, "Puyoul"));
        Produto produto = new Produto(1, "Feijão");
        venda.adicionarItem(produto , 1 , 10 ) ; 
        
        double resultado = venda.getTotal() ; 
        
        assertEquals(resultado, 10 , 0.05);
        
    }
    

}
